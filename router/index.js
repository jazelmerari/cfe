import { Router } from "express";

const router = Router();



router.post('/', (req, res) => {
    const params = {
        numero: req.body.numero,
        nombre: req.body.nombre,
        adress: req.body.adress,
        servicio: req.body.servicio,
        kilo: req.body.kilo,
    }
    res.render('index', params);
})

router.get('/', (req, res) => {
    const params = {
        numero: req.query.numero,
        nombre: req.query.nombre,
        adress: req.query.adress,
        servicio: req.query.servicio,
        kilo: req.query.kilo,

    }

    res.render('index', params)
})

export default router;